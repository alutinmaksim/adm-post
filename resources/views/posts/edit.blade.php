@extends('layouts.layout')
@section('title', 'Изменить пост')
@section('content')
    <div class="container">
        <div class="my-3">
            Изменить пост
        </div>
        <form action="{{ route('posts.update', $post->id) }}" enctype="multipart/form-data" method="post">
            @csrf
            @method('patch')
            <div class="input-group my-3">
                <input name="image" type="file" class="form-control" id="inputGroupFile04" aria-describedby="inputGroupFileAddon04" aria-label="Загрузка" >
            </div>
            <div class="w-25">
                <img src="{{ url('storage/' . $post->image) }}" class="w-50">
            </div>
            <div class="my-3">
                <label for="exampleFormControlInput1" class="form-label">Название статьи</label>
                <input name="title" type="text" class="form-control" id="exampleFormControlInput1" placeholder="title" value="{{ $post->title }}">
            </div>
            <div class="mb-3">
                <label for="exampleFormControlTextarea1" class="form-label">Контент</label>
                <textarea name="content" class="form-control" id="exampleFormControlTextarea1" rows="3" placeholder="content">{{ $post->content }}
                </textarea>
            </div>
            <div class="col-12">
                <button type="submit" class="btn btn-primary">Изменить</button>
            </div>
            <a href="{{ route('posts.show', $post->id) }}" class="btn btn-outline-success btn-sm mt-2">Отменить</a>
        </form>
    </div>
@endsection
