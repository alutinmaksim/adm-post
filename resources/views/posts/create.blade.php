@extends('layouts.layout')
@section('title', 'Создать пост')
@section('content')
    <div class="container">
        <div class="my-3">
            Новый пост
        </div>
        <form action="{{ route('posts.store') }}" enctype="multipart/form-data" method="post">
            @csrf
            <div class="input-group my-3">
                <input name="image" type="file" class="form-control" id="inputGroupFile04" aria-describedby="inputGroupFileAddon04" aria-label="Загрузка">
            </div>
        <div class="my-3">
            <label for="exampleFormControlInput1" class="form-label">Название статьи</label>
            <input name="title" type="text" class="form-control" id="exampleFormControlInput1" placeholder="title">
        </div>
        <div class="mb-3">
            <label for="exampleFormControlTextarea1" class="form-label">Контент</label>
            <textarea name="content" class="form-control" id="exampleFormControlTextarea1" rows="3" placeholder="content"></textarea>
        </div>
            <div class="col-12">
                <button type="submit" class="btn btn-primary">Сохранить</button>
            </div>
        </form>
    </div>
@endsection
