@extends('layouts.layout')
@section('title', 'Все посты')
@section('content')
    @include('alert.success')
    <a href="{{ route('logout') }}" class="btn btn-outline-danger btn-sm mr-2">Выйти</a>
    @can('create', \App\Models\Post::class)
    <a href="{{ route('posts.create') }}" class="btn btn-outline-success btn-sm">Новый пост</a>
    <a href="{{ route('posts.unpublished') }}" class="btn btn-outline-success btn-sm">Неопубликованное</a>
    @endcan
    <div class="container col-md-6">
        @if(count($posts) > 0)
            @foreach($posts as $post)
                <div class="">
                    <div class="card mb-3">
                        <img src="{{ url('storage/' . $post->image) }}" class="card-img-top" alt="...">
                        <div class="card-body">
                            <h5 class="card-title">{{ $post->title }}</h5>
                            <p class="card-text">{{ \Illuminate\Support\Str::words($post->content, 10) }}</p>

                            <p class="card-text"><small class="text-body-secondary">Последнее
                                    обновление {{ $post->created_at->diffForHumans() }}</small>
                            </p>
                        </div>
                        <a href="{{ route('posts.show', $post->id) }}" class="btn btn-outline-info btn-sm">Читать далее...</a>
                    </div>
                    <hr>
                    @endforeach
                    @else
                        <div>
                            Постов пока еще нет...
                        </div>
                    @endif
                </div>
    </div>

@endsection
