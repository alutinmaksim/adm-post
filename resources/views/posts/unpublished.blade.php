@extends('layouts.layout')
@section('title', 'Неопубликованное')
@section('content')
    <a href="{{ route('logout') }}" class="btn btn-outline-danger btn-sm mr-2">Выйти</a>
    <a href="{{ route('posts.index') }}" class="btn btn-outline-info btn-sm">На главную</a>
    <div class="container col-md-6">
        @if(count($posts) > 0)
            @foreach($posts as $post)
                <div class="">
                    <div class="card mb-3">
                        <img src="{{ url('storage/' . $post->image) }}" class="card-img-top" alt="...">
                        <div class="card-body">
                            <h5 class="card-title">{{ $post->title . ' (id: ' . $post->id . ')' }}</h5>
                            <p class="card-text">{{ $post->content }}</p>

                            <p class="card-text"><small class="text-body-secondary">Последнее
                                    обновление {{ $post->created_at->diffForHumans() }}</small>
                            </p>
                        </div>
                        <a href="{{ route('posts.edit', $post->id) }}" class="btn btn-outline-success btn-sm">Изменить</a>
                    </div>
                    <hr>
                    @endforeach
                    @else
                        <div>
                            Постов пока еще нет...
                        </div>
                    @endif
                </div>
    </div>

@endsection
