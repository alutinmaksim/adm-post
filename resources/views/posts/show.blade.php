@extends('layouts.layout')
@section('title', 'Обзор поста')
@section('content')
    @include('alert.success')

    <div class="container col-md-6">
        <div class="card mb-3">
            <img src="{{ url('storage/' . $post->image) }}" class="card-img-top" alt="...">
            <div class="card-body">
                <h3 class="card-title">{{ $post->title }}</h3>
                <p class="card-text">{{ $post->content }}</p>
                <p class="card-text"><small class="text-body-secondary">Последнее
                        обновление {{ $post->created_at->diffForHumans() }}</small>
                </p>
            </div>
            <a href="{{ route('posts.index') }}" class="btn btn-outline-info btn-sm">На главную</a>
            @canany(['update', 'delete'], $post)
                <a href="{{ route('posts.edit', $post->id) }}" class="btn btn-outline-warning btn-sm">Изменить</a>
                <form action="{{ route('posts.destroy', $post->id) }}" method="post">
                    @csrf
                    @method('delete')
                    <button class="btn btn-outline-danger btn-sm" type="submit">Удалить</button>
                </form>
            @endcan
        </div>
    </div>

@endsection
