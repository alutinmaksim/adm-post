@extends('layouts.layout')
@section('title', 'Вход')
@section('content')
    <div class="container">
        <a href="{{ route('view-registration-form') }}">
            <i>Регистрация</i>
        </a>
    </div>
<div class="container col-6 mt-5">
    <div class="register-logo">
        <b>Войти</b>
    </div>
    <div class="card">
        <div class="card-body register-card-body">
            <form class="row g-3" action="{{ route('login') }}" method="post">
                @csrf
                <div class="col-md-6">
                    <label for="inputEmail4" class="form-label">Email</label>
                    <input type="email"  name="email" class="form-control @error('email') is-invalid @enderror" id="inputEmail4">
                    @error('email')
                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                    @enderror
                </div>
                <div class="col-md-6">
                    <label for="inputPassword4" class="form-label">Password</label>
                    <input type="password" name="password" class="form-control @error('password') is-invalid @enderror" id="inputPassword4">
                    @error('password')
                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                    @enderror
                </div>
                <div class="col-12">
                    <button type="submit" class="btn btn-primary">Sign in</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

