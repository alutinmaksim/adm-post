<?php

namespace App\Http\Services;

use App\Models\Post;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Storage;

class PostService
{

    public function getAllForIndex(): Collection|array
    {
        $posts = Post::query()
            ->where('is_published', 1)
            ->orderBy('created_at', 'DESC')
            ->get();
        return $posts;
    }

    public function getUnpublished(): Collection|array
    {
        $posts = Post::query()
            ->where('is_published', 0)
            ->orderBy('created_at', 'DESC')
            ->get();
        return $posts;
    }

    public function store(array $data): void
    {
        if (isset($data['image'])) {
            $data['image'] = Storage::disk('public')->put('/images', $data['image']);
        }
        Post::query()->create($data);
    }

    public function update(array $data, Post $post): void
    {
        if (isset($data['image'])) {
            $data['image'] = Storage::disk('public')->put('/images', $data['image']);
        }
        $post->update($data);
    }

    public function publishPost($id): bool
    {
        $post = Post::query()->where('id', $id)->first();
        if ($post) {
            $post->is_published = 1;
            $post->save();
            return true;
        }
        return false;
//dd($id);
//            $post = Post::query()->where('id', $id)->firstOrFail();
//            $post->is_published = 1;
//            $post->save();
//            return $post;

    }

}
