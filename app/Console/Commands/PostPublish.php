<?php

namespace App\Console\Commands;

use App\Http\Services\PostService;
use Illuminate\Console\Command;

class PostPublish extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'post:publish {--id=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command can publish posts using the --id parameter';

    /**
     * Execute the console command.
     */
    public function handle(PostService $service): void
    {
        $postId = $this->option('id');
        $res = $service->publishPost($postId);
        if ($res) {
            $this->info('The post was published successfully');
        } else {
            $this->error('The post could not be found and published');
        }
    }
}
