<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\PostController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::group(['middleware' => 'guest'], function () {
    Route::get('/', [AuthController::class, 'loginForm'])->name('view-login-form');
    Route::post('/', [AuthController::class, 'login'])->name('login');
    Route::get('/registration', [AuthController::class, 'registrationForm'])->name('view-registration-form');
    Route::post('/registration', [AuthController::class, 'registration'])->name('registration');
});

Route::get('/logout', [AuthController::class, 'logout'])->name('logout');

Route::group(['prefix' => 'posts', 'middleware' => 'auth'], function () {
    Route::get('/', [PostController::class, 'index'])->name('posts.index');
    Route::get('/unpublished', [PostController::class, 'unpublished'])->name('posts.unpublished')->can('unpublished', '\App\Models\Post');
    Route::get('/create', [PostController::class, 'create'])->name('posts.create')->can('create','\App\Models\Post');
    Route::post('/create', [PostController::class, 'store'])->name('posts.store')->can('create','\App\Models\Post');
    Route::get('/{post}', [PostController::class, 'show'])->name('posts.show');
    Route::get('/{post}/edit', [PostController::class, 'edit'])->name('posts.edit')->can('update','post');
    Route::patch('/{post}', [PostController::class, 'update'])->name('posts.update')->can('update','post');
    Route::delete('/{post}', [PostController::class, 'destroy'])->name('posts.destroy')->can('delete','post');
});

Route::fallback(function () {
    return redirect()->to('/posts');
});
